"""
Module to run crawler from Python code.


Commands for future reference:

- Create new scrapy project:
    scrapy startproject <project name>

- Scrapy shell:
    scrapy shell "<url to scrape>"

- Run crawler (n project directory):
    scrapy crawl <spider unique 'name'> -o <output file> [-a <attribute=value>]

"""

from scrapy.crawler import CrawlerProcess
from homework4.spiders import www_osta_ee
from scrapy.utils.project import get_project_settings


if __name__ == '__main__':
    settings = get_project_settings()
    settings.set('FEED_FORMAT', 'json')
    settings.set('FEED_URI', 'www_osta_ee_arvutid.json')
    # settings.set('FEED_EXPORT_INDENT', 2)  # commented out for compact output

    process = CrawlerProcess(settings)
    process.crawl(www_osta_ee.CategorySpider, category='arvutid')
    process.start()
