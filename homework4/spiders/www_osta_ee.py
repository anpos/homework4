"""
Spider for www.osta.ee to browse specific category and parse following data for each item:

- Title: item title
- Price: current price or buy now price
- Picture href: image file url

"""

import scrapy


class CategorySpider(scrapy.Spider):
    name = "www_osta_ee_category_spider"

    def start_requests(self):
        category = getattr(self, 'category', None)
        url = 'http://www.osta.ee/'
        if category:
            url += 'kategooria/' + category
        yield scrapy.Request(url, self.parse)

    def parse(self, response):
        listing = response.css('li figure.new-th')
        for item in listing:
            current_price = item.css('div footer div div span.price-cp::text').get()
            buy_now_price = item.css('div footer div div span.price-bn::text').get()
            yield {
                'Title': item.css('div div p::attr(title)').get(),
                'Price': current_price or buy_now_price,
                'Picture href': item.css('figure a img::attr(data-original)').get(),
            }

        next_page = response.xpath("//span[@id='nextPage']/../@href").get()
        if next_page:
            yield response.follow(next_page, self.parse)
